
# Avoid very slow download times
#
# See:
# - https://stackoverflow.com/questions/28682642/powershell-why-is-using-invoke-webrequest-much-slower-than-a-browser-download
# - https://github.com/PowerShell/PowerShell/issues/2138
# - https://github.com/PowerShell/PowerShell/issues/13414
$ProgressPreference = 'SilentlyContinue'


echo "Downloading CMake ..."
# Invoke-WebRequest -Uri "https://github.com/Kitware/CMake/releases/download/v3.20.5/cmake-3.20.5-windows-x86_64.msi" -OutFile "C:\TEMP\cmake_install.msi"
Invoke-WebRequest -Uri "https://github.com/Kitware/CMake/releases/download/v3.30.2/cmake-3.30.2-windows-x86_64.msi" -OutFile "C:\TEMP\cmake_install.msi"

if(!$?){
  Write-Output "Downloading CMake failed"
  exit 1
}


echo "Installing CMake ..."
Start-Process msiexec.exe -ArgumentList "/qb /i C:\TEMP\cmake_install.msi INSTALL_ROOT=C:\tools\cmake ADD_CMAKE_TO_PATH=System" -NoNewWindow -Wait

if(!$?){
  Write-Output "Installing CMake failed, code $LASTEXITCODE"
  exit 1
}
Write-Output "Installing CMake done"
