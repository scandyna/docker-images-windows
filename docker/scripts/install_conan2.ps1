
echo "Installing Conan ..."

pip install conan==2.6.0

if(!$?){
  Write-Output "Installing Conan failed, code $LASTEXITCODE"
  exit 1
}
Write-Output "Installing Conan done"
