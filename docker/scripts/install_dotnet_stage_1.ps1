
# Avoid very slow download times
#
# See:
# - https://stackoverflow.com/questions/28682642/powershell-why-is-using-invoke-webrequest-much-slower-than-a-browser-download
# - https://github.com/PowerShell/PowerShell/issues/2138
# - https://github.com/PowerShell/PowerShell/issues/13414
$ProgressPreference = 'SilentlyContinue'


# This script is inspired by Microsoft Dockerfile:
# https://github.com/microsoft/dotnet-framework-docker/blob/8305f377abae2cb51d2f9b3a451698d39d10e3c3/src/runtime/3.5/windowsservercore-ltsc2019/Dockerfile

echo "Downloading .NET Fx ..."
Invoke-WebRequest -Uri "https://dotnetbinaries.blob.core.windows.net/dockerassets/microsoft-windows-netfx3-ltsc2019.zip" -OutFile "C:\TEMP\dotnet_fx3.zip"
if(!$?){
  Write-Output "Downloading .NET Fx failed, code $LASTEXITCODE"
  exit 1
}

cd C:\TEMP

echo "Extracting .NET Fx ..."
tar -zxf dotnet_fx3.zip
if(!$?){
  Write-Output "Extracting .NET Fx failed, code $LASTEXITCODE"
  exit 1
}

echo "Installing .NET Fx ..."
dism /Online /Quiet /NoRestart /Add-Package /PackagePath:.\microsoft-windows-netfx3-ondemand-package~31bf3856ad364e35~amd64~~.cab
if(!$?){
  Write-Output "Installing .NET Fx failed, code $LASTEXITCODE"
  exit 1
}
