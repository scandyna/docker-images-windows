
echo "Installing Qt MinGW 8.1 toolchain ..."
python -m aqt install-tool --outputdir C:\Qt windows desktop tools_mingw qt.tools.win64_mingw810

if(!$?){
  Write-Output "Installing Qt MinGW failed, code $LASTEXITCODE"
  exit 1
}
Write-Output "Installing Qt MinGW done"
