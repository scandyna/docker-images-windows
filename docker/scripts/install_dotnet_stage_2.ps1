
# Avoid very slow download times
#
# See:
# - https://stackoverflow.com/questions/28682642/powershell-why-is-using-invoke-webrequest-much-slower-than-a-browser-download
# - https://github.com/PowerShell/PowerShell/issues/2138
# - https://github.com/PowerShell/PowerShell/issues/13414
$ProgressPreference = 'SilentlyContinue'

# NOTE: for some reason, running this script fails.
#
# Currently, .NET is installed from docker/windows-full-dotnet/

# This script is inspired by Microsoft Dockerfile:
# https://github.com/microsoft/dotnet-framework-docker/blob/8305f377abae2cb51d2f9b3a451698d39d10e3c3/src/runtime/3.5/windowsservercore-ltsc2019/Dockerfile


echo "Downloading .NET Fx patch ..."
Invoke-WebRequest -Uri "https://catalog.s.download.windowsupdate.com/c/msdownload/update/software/updt/2022/06/windows10.0-kb5015736-x64_596d4a58f9c8a9e6f661a3100e3284e33fbca197.msu" -OutFile "C:\TEMP\dotnet_fx3_patch.msu"
if(!$?){
  Write-Output "Downloading .NET Fx patch failed, code $LASTEXITCODE"
  exit 1
}

cd C:\TEMP

echo "Extracting .NET Fx patch ..."
mkdir patch
expand dotnet_fx3_patch.msu patch -F:*
if(!$?){
  Write-Output "Extracting .NET Fx patch failed, code $LASTEXITCODE"
  exit 1
}

echo "Installing .NET Fx patch ..."
dism /Online /NoRestart /Add-Package /PackagePath:C:\TEMP\patch\Windows10.0-KB5015736-x64.cab
if(!$?){
  Write-Output "Installing .NET Fx patch failed, code $LASTEXITCODE"

  Write-Output "Content of C:\Windows\Logs\DISM\dism.log:"
  Get-Content "C:\Windows\Logs\DISM\dism.log"

#   Write-Output "Note: ignoring .NET Fx patch install failure .."
  exit 1
}


echo "Calling ngen (can maybe produce errors) ..."

Start-Process -FilePath "${ENV:WINDIR}\Microsoft.NET\Framework64\v2.0.50727\ngen.exe" -ArgumentList "uninstall Microsoft.Tpm.Commands, Version=10.0.0.0, Culture=Neutral, PublicKeyToken=31bf3856ad364e35, processorArchitecture=amd64" -NoNewWindow -Wait

Start-Process -FilePath "${ENV:WINDIR}\Microsoft.NET\Framework64\v2.0.50727\ngen.exe" -ArgumentList "update" -NoNewWindow -Wait

Start-Process -FilePath "${ENV:WINDIR}\Microsoft.NET\Framework\v2.0.50727\ngen.exe" -ArgumentList "update" -NoNewWindow -Wait

echo "Installing .NET Fx and patch done"
