
# Avoid very slow download times
#
# See:
# - https://stackoverflow.com/questions/28682642/powershell-why-is-using-invoke-webrequest-much-slower-than-a-browser-download
# - https://github.com/PowerShell/PowerShell/issues/2138
# - https://github.com/PowerShell/PowerShell/issues/13414
$ProgressPreference = 'SilentlyContinue'

# Installing recent versions of aqt
# needs some build tools, like MSVC
# See:
# - https://gitlab.com/scandyna/docker-images-windows/-/jobs/2400226080
# - https://github.com/miurahr/aqtinstall

echo "Installing aqt (Another Qt installer) ..."
pip install aqtinstall

if(!$?){
  Write-Output "Installing aqt failed, code $LASTEXITCODE"
  exit 1
}
Write-Output "Installing aqt done"
