
# Avoid very slow download times
#
# See:
# - https://stackoverflow.com/questions/28682642/powershell-why-is-using-invoke-webrequest-much-slower-than-a-browser-download
# - https://github.com/PowerShell/PowerShell/issues/2138
# - https://github.com/PowerShell/PowerShell/issues/13414
$ProgressPreference = 'SilentlyContinue'

# Some story about installing a legacy Direct X SDK
# See: https://gitlab.com/scandyna/conan-qt-builds/-/issues/1
#
# Problems installing DX SDK, see https://gitlab.com/scandyna/docker-images-windows/-/issues/1

# This script is inspired by Qt CI scripts:
# https://code.qt.io/cgit/qt/qt5.git/tree/coin/provisioning/common/windows?h=5.15

echo "Downloading DX SDK ..."
Invoke-WebRequest -Uri "https://download.microsoft.com/download/A/E/7/AE743F1F-632B-4809-87A9-AA1BB3458E31/DXSDK_Jun10.exe" -OutFile "C:\TEMP\dxsdk_install.exe"

if(!$?){
  Write-Output "Downloading DX SDK failed"
  exit 1
}

# The DX SDK setup allways fails with exit code 1023
# Try uninstall Visual C++ 2010 Redistributable
# See https://docs.microsoft.com/en-us/troubleshoot/windows/win32/s1023-error-when-you-install-directx-sdk
echo "Uninstalling Visual C++ 2010 Redistributable ..."
Start-Process MsiExec.exe -ArgumentList "/passive /X{F0C3E5D1-1ADE-321E-8167-68EF0DE699A5}" -NoNewWindow -Wait
Start-Process MsiExec.exe -ArgumentList "/passive /X{1D8E6291-B0D5-35EC-8441-6616F567A0F7}" -NoNewWindow -Wait


echo "Installing DX SDK ..."
$Installprocess = Start-Process -FilePath C:\TEMP\dxsdk_install.exe -ArgumentList "/U" -PassThru -NoNewWindow -Wait

if($Installprocess.ExitCode -ne 0){
  Write-Output "Installing DX SDK failed, code: $($Installprocess.ExitCode)"

  # Have no idea how to (simply) put log files outside a container
  # So, at least put those to the console, so we should maybe have some info

  Write-Output "Content of C:\Windows\Logs\DirectX_SDK.log:"
  Get-Content "C:\Windows\Logs\DirectX_SDK.log"

  Write-Output "Content of C:\Windows\Logs\DirectX.log:"
  Get-Content "C:\Windows\Logs\DirectX.log"

#   Write-Output "Note: ignoring errors while installing DX SDK"
  exit $Installprocess.ExitCode
}

Write-Output "Installing DX SDK done"
