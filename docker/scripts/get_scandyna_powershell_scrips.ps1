$ErrorActionPreference = 'Stop'

Write-Output "Getting helper scripts from scandyna/powershell-scripts repo ..."

Invoke-WebRequest -Uri "https://gitlab.com/api/v4/projects/39882706/packages/generic/powershell_scripts/0.0.8/powershell_scripts.zip" -OutFile "C:\TEMP\powershell_scripts.zip" -ErrorAction Stop
Expand-Archive -Path "C:\TEMP\powershell_scripts.zip" -DestinationPath "C:\TEMP\" -ErrorAction Stop

ls C:\TEMP\

Write-Output "Getting helper scripts from scandyna/powershell-scripts repo done"
