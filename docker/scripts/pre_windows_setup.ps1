#
# Some common Windows setup
#
# Some parts have been inspired from Qt COIN:
# https://code.qt.io/cgit/qt/qt5.git/tree/coin
#

$ErrorActionPreference = 'Stop'

#################################
# Enable long path support
#################################

Write-Output "Enabling long paths support"

Set-ItemProperty -Path Registry::"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\FileSystem" -Name "LongPathsEnabled" -Value 1

####################################################################
# See https://gitlab.com/scandyna/docker-images-windows/-/issues/7
####################################################################

Write-Output "Mapping font MS Shell Dlg 2 ..."

#Set-ItemProperty -Path Registry::"HKEY_LOCAL_MACHINE\Software\Microsoft\Windows NT\CurrentVersion\FontSubstitutes" -Name "MS Shell Dlg 2" -Value "Microsoft Sans Serif"
Set-ItemProperty -Path Registry::"HKEY_LOCAL_MACHINE\Software\Microsoft\Windows NT\CurrentVersion\FontSubstitutes" -Name "MS Shell Dlg 2" -Value "Lucida Console"

if(!$?){
  Write-Output "Mapping font MS Shell Dlg 2 failed"
  exit 1
}

Write-Output "Mapping font MS Shell Dlg 2 done"

####################################
# Disable some Windows
# tasks that slows the machine down
####################################

C:\TEMP\disable_disk_cleanup.ps1
C:\TEMP\disable_defragment.ps1
C:\TEMP\disable_windefender.ps1
