
# Avoid very slow download times
#
# See:
# - https://stackoverflow.com/questions/28682642/powershell-why-is-using-invoke-webrequest-much-slower-than-a-browser-download
# - https://github.com/PowerShell/PowerShell/issues/2138
# - https://github.com/PowerShell/PowerShell/issues/13414
$ProgressPreference = 'SilentlyContinue'

echo "Downloading Python ..."
# Invoke-WebRequest -Uri "https://www.python.org/ftp/python/3.8.5/python-3.8.5.exe" -OutFile "C:\TEMP\python_install.exe"
# Invoke-WebRequest -Uri "https://www.python.org/ftp/python/3.8.5/python-3.8.5-amd64.exe" -OutFile "C:\TEMP\python_install.exe"
Invoke-WebRequest -Uri "https://www.python.org/ftp/python/3.12.5/python-3.12.5-amd64.exe" -OutFile "C:\TEMP\python_install.exe"

if(!$?){
  Write-Output "Downloading Python failed"
  exit 1
}


echo "Installing Python ..."
Start-Process -FilePath C:\TEMP\python_install.exe -ArgumentList "/quiet InstallAllUsers=1 TargetDir=C:\tools\python3.12 PrependPath=1 Include_doc=0 Include_test=0" -NoNewWindow -Wait

if(!$?){
  Write-Output "Installing Python failed, code $LASTEXITCODE"
  exit 1
}

Write-Output "Installing Python done"

# NOTE: do not call python from here, it will not be in the PATH
# See also remarks in Dockerfile
