
# Avoid very slow download times
#
# See:
# - https://stackoverflow.com/questions/28682642/powershell-why-is-using-invoke-webrequest-much-slower-than-a-browser-download
# - https://github.com/PowerShell/PowerShell/issues/2138
# - https://github.com/PowerShell/PowerShell/issues/13414
$ProgressPreference = 'SilentlyContinue'

echo "Downloading Boost ..."
Invoke-WebRequest -Uri "https://boostorg.jfrog.io/artifactory/main/release/1.73.0/source/boost_1_73_0.7z" -OutFile "C:\TEMP\boost.7z"

if(!$?){
  Write-Output "Downloading Boost failed"
  exit 1
}


echo "Installing Boost ..."
7z x "C:\TEMP\boost.7z" -o"C:\Libraries\"

if(!$?){
  Write-Output "Installing Boost failed, code $LASTEXITCODE"
  exit 1
}
Write-Output "Installing Boost done"
