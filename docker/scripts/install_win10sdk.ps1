
# Avoid very slow download times
#
# See:
# - https://stackoverflow.com/questions/28682642/powershell-why-is-using-invoke-webrequest-much-slower-than-a-browser-download
# - https://github.com/PowerShell/PowerShell/issues/2138
# - https://github.com/PowerShell/PowerShell/issues/13414
$ProgressPreference = 'SilentlyContinue'

# This script installs the Windows 10 SDK without MSVC
#
# To use MSVC, se install_msvc.ps1, which also installs the Windows 10 SDK

# This script is inspired by Qt CI scripts:
# https://code.qt.io/cgit/qt/qt5.git/tree/coin/provisioning/common/windows?h=5.15

echo "Downloading Windows 10 SDK ..."
Invoke-WebRequest -Uri "https://download.microsoft.com/download/8/C/3/8C37C5CE-C6B9-4CC8-8B5F-149A9C976035/windowssdk/winsdksetup.exe" -OutFile "C:\TEMP\winsdksetup.exe"

if(!$?){
  Write-Output "Downloading Windows 10 SDK failed"
  exit 1
}

echo "Installing Windows 10 SDK ..."
Start-Process -FilePath C:\TEMP\winsdksetup.exe -ArgumentList "/features + /q" -NoNewWindow -Wait

if(!$?){
  Write-Output "Installing Windows 10 SDK failed, code $LASTEXITCODE"
  exit 1
}

Write-Output "Installing Windows 10 SDK done"
