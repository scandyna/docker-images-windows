
echo "Updating pip and some tools ..."
python -m pip install --upgrade pip setuptools wheel

if(!$?){
  Write-Output "Updating pip and tools failed, code $LASTEXITCODE"
  exit 1
}

echo "Updates done"

# TODO: try to not use aqt anymore
# Installing aqt recently failed
# See for example https://gitlab.com/scandyna/docker-images-windows/-/jobs/2400365608
# It seems that zipfile-deflate64 extension requires MSVC to build it,
# which is a no-go to have in this image (images are already fat enough)
# Looking at https://pypi.org/project/zipfile-deflate64/#files ,
# Windows binaries exists.
# The pip documentation says that compatible binaries are preffered:
# https://packaging.python.org/en/latest/tutorials/installing-packages/#source-distributions-vs-wheels
# After trying locally, I think this is maybe wrong now
# So, try install zipfile-deflate64 forcing it to choose binary package
# echo "Installing some Python packages ..."
# python -m pip install --only-binary=:zipfile-deflate64:  zipfile-deflate64
# python -m pip install --only-binary=:all:  zipfile-deflate64
# python -m pip install --prefer-binary  zipfile-deflate64

# if(!$?){
#   Write-Output "Installing Python packages failed, code $LASTEXITCODE"
#   exit 1
# }

# echo "Installing Python packages done"
