
echo "Installing Conan ..."
# Now, by default, conan 2 will be installed. We are not ready for that yet
pip install conan==1.64.1
conan config set general.revisions_enabled=1

if(!$?){
  Write-Output "Installing Conan failed, code $LASTEXITCODE"
  exit 1
}
Write-Output "Installing Conan done"
