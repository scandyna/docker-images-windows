
# Install minGW tools
# For the version (like 7.3.0-1-202004170606)
# and associated qt.tools.x (like qt.tools.win64_mingw730),
# see the components.xml of a installed and up to date Qt installation made with official Qt installer
# The supported qt.tools.x can be checked here: https://github.com/miurahr/aqtinstall/blob/master/aqt/combinations.json
#
# Note: currently, aqt will not fail if the required tool cannot be fetched, it simply installs nothing
# (As stated by the authors, this is a experimental feature)
#
# Simplified syntax: aqt tool {linux,mac,windows} tool_name version arch
#
# Update 20210407:
# - the complete version, like 7.3.0-1-202004170606, seems not to be required anymore.
# - qt.tools.win64_mingw730 does no longer install the 32bit version
#   qt.tools.win32_mingw730 is now available for the 32bit version
#
# Upadte 20211121:
# aqt now provides a official tools installer.
# https://aqtinstall.readthedocs.io/en/stable/getting_started.html#installing-tools
# (previously used commands did no longer work)
#

echo "Installing Qt MinGW 7.3 toolchain ..."
# python -m aqt tool --outputdir C:\Qt windows tools_mingw 7.3.0-1-202004170606 qt.tools.win64_mingw730
# python -m aqt tool --outputdir C:\Qt windows tools_mingw 7.3.0 qt.tools.win32_mingw730
# python -m aqt tool --outputdir C:\Qt windows tools_mingw 7.3.0 qt.tools.win64_mingw730
python -m aqt install-tool --outputdir C:\Qt windows desktop tools_mingw qt.tools.win32_mingw730

if(!$?){
  Write-Output "Installing Qt MinGW failed, code $LASTEXITCODE"
  exit 1
}
Write-Output "Installing Qt MinGW done"
