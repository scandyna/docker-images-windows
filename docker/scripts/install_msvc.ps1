
# Avoid very slow download times
#
# See:
# - https://stackoverflow.com/questions/28682642/powershell-why-is-using-invoke-webrequest-much-slower-than-a-browser-download
# - https://github.com/PowerShell/PowerShell/issues/2138
# - https://github.com/PowerShell/PowerShell/issues/13414
$ProgressPreference = 'SilentlyContinue'


Write-Output "Downloading VsCollect ..."
Invoke-WebRequest -Uri "https://aka.ms/vscollect.exe" -OutFile "C:\TEMP\vs_collect.exe"

if(!$?){
  Write-Output "Downloading VsCollect failed"
  exit 1
}


echo "Downloading MSVC ..."
Invoke-WebRequest -Uri "https://aka.ms/vs/16/release/vs_buildtools.exe" -OutFile "C:\TEMP\vs_buildtools.exe"

if(!$?){
  Write-Output "Downloading MSVC failed"
  exit 1
}


# TODO: add --lang en-US () For this, see layout option) ?

echo "Installing MSVC ..."
$Installprocess = Start-Process -FilePath C:\TEMP\vs_buildtools.exe -ArgumentList "--quiet --wait --norestart --nocache --add Microsoft.VisualStudio.Workload.VCTools --add Microsoft.VisualStudio.Component.VC.Tools.x86.x64 --add Microsoft.VisualStudio.Component.Windows10SDK.19041" -NoNewWindow -Wait -PassThru

if($Installprocess.ExitCode -ne 0){
  Write-Output "Installing MSVC failed, code: $($Installprocess.ExitCode)"

  Write-Output "Collecting install logs ..."
  Start-Process -FilePath C:\TEMP\vs_collect.exe -ArgumentList "-zip:C:\vs_install_logs.zip" -Wait
  Write-Output "Collecting install logs finished, file: C:\vs_install_logs.zip"

  exit $Installprocess.ExitCode
}

Write-Output "Installing MSVC done"
