# Avoid very slow download times
#
# See:
# - https://stackoverflow.com/questions/28682642/powershell-why-is-using-invoke-webrequest-much-slower-than-a-browser-download
# - https://github.com/PowerShell/PowerShell/issues/2138
# - https://github.com/PowerShell/PowerShell/issues/13414
$ProgressPreference = 'SilentlyContinue'

# Getting MinGW builds is not so easy.
# There are many projects that provides binaries.
# See: https://www.mingw-w64.org/downloads/
# In the list of avaliable downloads, some provides 7z archives.
# The only problem: they also provide tools, like CMake, Vim.
# We don't want them here (also avoid conflicts).
#
# To get a link where Qt grabs MinGW, see at there scripts in QtCoIn:
# https://code.qt.io/cgit/qt/qt5.git/tree/coin/provisioning
# Choose a 5.15 branch to have the one that was previously installed with aqt
#

echo "Downloading MinGW ..."
$url = "https://netcologne.dl.sourceforge.net/project/mingw-w64/Toolchains%20targetting%20Win64/Personal%20Builds/mingw-builds/8.1.0/threads-posix/seh/x86_64-8.1.0-release-posix-seh-rt_v6-rev0.7z"
Invoke-WebRequest -Uri $url -OutFile "C:\TEMP\mingw.7z"
if(!$?){
  Write-Output "Downloading MinGW failed"
  exit 1
}

echo "Installing MinGW ..."
7z x "C:\TEMP\mingw.7z" -o"C:\MinGW"
if(!$?){
  Write-Output "Installing MinGW failed, code $LASTEXITCODE"
  exit 1
}

Write-Output "Current PATH: $Env:Path"
Write-Output "Adding MinGW to PATH ..."
setx /M PATH "C:\MinGW\mingw64\bin;$Env:Path"
Write-Output "PATH: $Env:Path"

Write-Output "Installing MinGW done"
