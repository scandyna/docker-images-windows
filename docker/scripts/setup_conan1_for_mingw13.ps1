
# We have to tell conan where the compilers are located.
# This will prevent confusions when a package is built with MSYS2
# See: https://gitlab.com/scandyna/docker-images-windows/-/issues/11
#
# I don't know how to setup a global.conf with conan commands.
# At least for conan 1, conan config set only works for conan.conf.
# And, the tool.* must be in global.conf
#
# NOTE: the install path is defined in install_mingw13.ps1
#

# Write-Output "Writing a global.conf for conan MinGW ..."
#
# Add-Content -Path "$HOME\.conan\global.conf" -Encoding utf8 -Value "tools.build:compiler_executables={'c':'C:/MinGW/mingw64/bin/gcc.exe','cpp':'C:/MinGW/mingw64/bin/g++.exe'}"
#
# if(!$?){
#   Write-Output "Writing global.conf failed"
#   exit 1
# }
#
# Write-Output "Writing global.conf done"

# We have to tell conan where the compilers are located.
# This will prevent confusions when a package is built with MSYS2
# See: https://gitlab.com/scandyna/docker-images-windows/-/issues/11
#
# We also have to enforce Conan generators to put our MinGW in front of the PATH
# See: https://gitlab.com/scandyna/docker-images-windows/-/issues/12
#
# Because we need a profile anyway, don't generate a global.conf
# This will prevent clashes when migrating to Conan 2
#

$conanProfileName = "windows_gcc13_x86_64_env.jinja"

$conanProfileContent = "[buildenv]`n"`
                     + "PATH=+(path)/MinGW/mingw64/bin`n"`
                     + "`n"`
                     + "[runenv]`n"`
                     + "PATH=+(path)/MinGW/mingw64/bin`n"`
                     + "`n"`
                     + "[conf]`n"`
                     + "tools.build:compiler_executables={'c':'C:/MinGW/mingw64/bin/gcc.exe','cpp':'C:/MinGW/mingw64/bin/g++.exe'}"


Write-Output "Writing $conanProfileName conan profile for MinGW ..."

# Juste after install of conan, the profiles dir does not exist
# See: https://gitlab.com/scandyna/docker-images-windows/-/jobs/7825227712
New-Item -Path "$HOME\.conan\profiles\$conanProfileName" -Force

Add-Content -Path "$HOME\.conan\profiles\$conanProfileName" -Encoding utf8 -Value $conanProfileContent

if(!$?){
  Write-Output "Writing profile failed"
  exit 1
}

Write-Output "Writing profile done"
