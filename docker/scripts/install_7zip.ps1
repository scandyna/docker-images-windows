
# Avoid very slow download times
#
# See:
# - https://stackoverflow.com/questions/28682642/powershell-why-is-using-invoke-webrequest-much-slower-than-a-browser-download
# - https://github.com/PowerShell/PowerShell/issues/2138
# - https://github.com/PowerShell/PowerShell/issues/13414
$ProgressPreference = 'SilentlyContinue'


echo "Downloading 7-Zip ..."
Invoke-WebRequest -Uri "https://www.7-zip.org/a/7z2107-x64.exe" -OutFile "C:\TEMP\7zip_install.exe"

if(!$?){
  Write-Output "Downloading 7-Zip failed"
  exit 1
}


echo "Installing 7-Zip ..."
Start-Process -FilePath C:\TEMP\7zip_install.exe -ArgumentList "/S" -NoNewWindow -Wait

if(!$?){
  Write-Output "Installing 7-Zip failed, code $LASTEXITCODE"
  exit 1
}

setx PATH "%PATH%;C:\Program Files\7-Zip"

if(!$?){
  Write-Output "Adding 7-Zip to PATH failed, code $LASTEXITCODE"
  exit 1
}

Write-Output "Installing 7-Zip done"
