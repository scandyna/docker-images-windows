# escape=`

# Microsoft provides .NET Docker images:
# - https://hub.docker.com/_/microsoft-dotnet-framework
# - https://github.com/microsoft/dotnet-framework-docker
#
# But, it seems to support only servercore
#
# Also, intalling the DX SDK could maybe not work on servercore:
# - https://docs.microsoft.com/en-us/virtualization/windowscontainers/deploy-containers/gpu-acceleration
#
# For details about the problem, see:
# - https://gitlab.com/scandyna/docker-images-windows/-/issues/1
#
# For the origin of the problem, see:
# - https://gitlab.com/scandyna/conan-qt-builds/-/issues/1
#
#
# This file is inspired from:
# server:ltsc2022: https://github.com/microsoft/dotnet-framework-docker/blob/main/src/runtime/3.5/windowsservercore-ltsc2022/Dockerfile
# windows:ltsc2019: https://github.com/microsoft/dotnet-framework-docker/blob/8305f377abae2cb51d2f9b3a451698d39d10e3c3/src/runtime/3.5/windowsservercore-ltsc2019/Dockerfile
#

# FROM mcr.microsoft.com/windows/server:ltsc2022
FROM mcr.microsoft.com/windows:ltsc2019

# Restore the default Windows shell for correct batch processing.
SHELL ["cmd", "/S", "/C"]

COPY scripts/* C:/TEMP/

# See https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_execution_policies?view=powershell-7.2
RUN powershell.exe Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope LocalMachine

RUN powershell.exe C:/TEMP/get_scandyna_powershell_scrips.ps1 && `
    powershell.exe C:/TEMP/pre_windows_setup.ps1 && `
    powershell.exe C:/TEMP/post_install_clean.ps1

# RUN powershell.exe Set-ItemProperty -Path Registry::"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\FileSystem" -Name "LongPathsEnabled" -Value 1

# For server:ltsc2022
# https://github.com/microsoft/dotnet-framework-docker/blob/main/src/runtime/3.5/windowsservercore-ltsc2022/Dockerfile
# ENV `
#     # Enable detection of running in a container
#     DOTNET_RUNNING_IN_CONTAINER=true `
#     COMPLUS_NGenProtectedProcess_FeatureEnabled=0

# RUN `
#     # Install .NET Fx 3.5
#     curl -fSLo microsoft-windows-netfx3.zip https://dotnetbinaries.blob.core.windows.net/dockerassets/microsoft-windows-netfx3-ltsc2022.zip `
#     && tar -zxf microsoft-windows-netfx3.zip `
#     && del /F /Q microsoft-windows-netfx3.zip `
#     && dism /Online /Quiet /Add-Package /PackagePath:.\microsoft-windows-netfx3-ondemand-package~31bf3856ad364e35~amd64~~.cab `
#     && del microsoft-windows-netfx3-ondemand-package~31bf3856ad364e35~amd64~~.cab `
#     && powershell Remove-Item -Force -Recurse ${Env:TEMP}\* `
#     `
#     # Apply latest 3.5 patch
#     && curl -fSLo patch.msu https://catalog.s.download.windowsupdate.com/d/msdownload/update/software/secu/2022/09/windows10.0-kb5017316-x64_f80e106e113fe039e5dd39e9b7b2c61922983598.msu `
#     && mkdir patch `
#     && expand patch.msu patch -F:* `
#     && del /F /Q patch.msu `
#     && dism /Online /Quiet /Add-Package /PackagePath:C:\patch\windows10.0-kb5017316-x64.cab `
#     && rmdir /S /Q patch `
#     `
#     # Ngen top of assembly graph to optimize a set of frequently used assemblies
#     && %windir%\Microsoft.NET\Framework64\v4.0.30319\ngen install "Microsoft.PowerShell.Utility.Activities, Version=3.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" `
#     # To optimize 32-bit assemblies, uncomment the next line
#     # && %windir%\Microsoft.NET\Framework\v4.0.30319\ngen install "Microsoft.PowerShell.Utility.Activities, Version=3.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" `
#     && %windir%\Microsoft.NET\Framework64\v4.0.30319\ngen update `
#     && %windir%\Microsoft.NET\Framework\v4.0.30319\ngen update

# For windows:ltsc2019
# https://github.com/microsoft/dotnet-framework-docker/blob/main/src/runtime/3.5/windowsservercore-ltsc2019/Dockerfile

ENV COMPLUS_NGenProtectedProcess_FeatureEnabled=0

RUN `
    # Install .NET Fx 3.5
    curl -fSLo microsoft-windows-netfx3.zip https://dotnetbinaries.blob.core.windows.net/dockerassets/microsoft-windows-netfx3-ltsc2019.zip `
    && tar -zxf microsoft-windows-netfx3.zip `
    && del /F /Q microsoft-windows-netfx3.zip `
    && dism /Online /Quiet /Add-Package /PackagePath:.\microsoft-windows-netfx3-ondemand-package~31bf3856ad364e35~amd64~~.cab `
    && del microsoft-windows-netfx3-ondemand-package~31bf3856ad364e35~amd64~~.cab `
    && powershell Remove-Item -Force -Recurse ${Env:TEMP}\* `
    `
    # Apply latest patch
    && curl -fSLo patch.msu https://catalog.s.download.windowsupdate.com/c/msdownload/update/software/updt/2022/06/windows10.0-kb5015736-x64_596d4a58f9c8a9e6f661a3100e3284e33fbca197.msu `
    && mkdir patch `
    && expand patch.msu patch -F:* `
    && del /F /Q patch.msu `
    && dism /Online /Quiet /Add-Package /PackagePath:C:\patch\windows10.0-kb5015736-x64.cab `
    && rmdir /S /Q patch `
    `
    # ngen .NET Fx
    && %windir%\Microsoft.NET\Framework64\v2.0.50727\ngen uninstall "Microsoft.Tpm.Commands, Version=10.0.0.0, Culture=Neutral, PublicKeyToken=31bf3856ad364e35, processorArchitecture=amd64" `
    && %windir%\Microsoft.NET\Framework64\v2.0.50727\ngen update `
    && %windir%\Microsoft.NET\Framework\v2.0.50727\ngen update
