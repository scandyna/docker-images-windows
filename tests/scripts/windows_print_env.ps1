
Write-Output "List some directories ..."
ls c:\
ls c:\tools
ls "c:\Program Files"
ls "c:\Program Files (x86)"

Write-Output "List PATH"
echo $env:PATH

Write-Output "Available fonts (HKEY_LOCAL_MACHINE\Software\Microsoft\Windows NT\CurrentVersion\Fonts):"
Get-ItemProperty -Path Registry::"HKEY_LOCAL_MACHINE\Software\Microsoft\Windows NT\CurrentVersion\Fonts"

Write-Output "Font substitutes (HKEY_LOCAL_MACHINE\Software\Microsoft\Windows NT\CurrentVersion\FontSubstitutes):"
Get-ItemProperty -Path Registry::"HKEY_LOCAL_MACHINE\Software\Microsoft\Windows NT\CurrentVersion\FontSubstitutes"

Write-Output "Long path (>260 chars) enabled (HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\FileSystem, LongPathsEnabled):"
Get-ItemProperty -Path Registry::"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\FileSystem" -Name LongPathsEnabled
