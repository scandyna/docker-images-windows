# docker-images-windows

Some Docker images used for GitLab CI/CD

[[_TOC_]]

# Usage

Currently (September 2020), GitLab.com does provide Windows shared runners,
but they do not support running docker images.
More information is available [here](https://about.gitlab.com/blog/2020/01/21/windows-shared-runner-beta/).

The first step is to install and register a runner,
like explained in the [GitLab doc](https://docs.gitlab.com/runner/install/windows.html).
Also register a runner with a [docker-windows](https://docs.gitlab.com/runner/executors/docker.html)
executor, and give him a tag that you remember (for example `windows-mypc-docker`).

Then a minimal example for Windows:
```yml
.windows_runner:
  tags:
    - windows-mypc-docker

stages:
  - build
  - test

build_windows_gcc_debug:
  stage: build
  image: registry.gitlab.com/scandyna/docker-images-windows/windows-cpp-qt-5.14.2-win64_mingw73:latest
  extends: .windows_runner
  script:
    - mkdir build
    - cd build
    - cmake -G"MinGW Makefiles" -DCMAKE_BUILD_TYPE=Debug
            -DCMAKE_PREFIX_PATH="C:/Libraries/boost_1_73_0;C:/Qt/5.14.2/mingw73_64" ..
    - cmake --build . --config Debug -j4
  artifacts:
    expire_in: 1 day
    paths:
      - build

test_windows_gcc_debug:
  stage: test
  image: registry.gitlab.com/scandyna/docker-images-windows/windows-cpp-qt-5.14.2-win64_mingw73:latest
  extends: .windows_runner
  dependencies:
    - build_windows_gcc_debug
  before_script:
    - $env:PATH += ";C:/Qt/5.14.2/mingw73_64/bin"
  script:
    - cd build
    - ctest --output-on-failure --build-config Debug .
```

Note: in above example we added the Qt bin directory
to the PATH, so the loader can find Qt's dll's to run the tests.
A possible alternative could be to handle those dll problems with CMake.
For example, use [MdtAddTest](https://scandyna.gitlab.io/mdt-cmake-modules/Modules/MdtAddTest.html),
which handle that as exaplained in the [MdtRuntimeEnvironment](https://scandyna.gitlab.io/mdt-cmake-modules/Modules/MdtRuntimeEnvironment.html) module.

# Available images

## Minimal images

### windows-minimal

A minimal image with some basic setup, like:
- Long paths support enabled
- PowerShell 7 (`pwsh`)

## Images to build and test c++ software

Some tools are available in each image.

Python >= 3.12 is installed, and in the `PATH`.

7-Zip is installed, and in the `PATH`.

CMake is installed, and in the `PATH`.

Conan is installed, and can be called directly.

Git is installed, and in the `PATH`.

In some final images, like windows-cpp-qt-5.14.2-win64_mingw73,
Boost 1.73 is installed in `C:\Libraries\boost_1_73_0`.

### windows-cpp-win64_mingw73

Specify the image:
```yml
image: registry.gitlab.com/scandyna/docker-images-windows/windows-cpp-win64_mingw73:latest
```

gcc 7.3 64bit is installed in `C:\Qt\Tools\mingw730_64\bin` and is in the `PATH`.

### windows-cpp-win32_mingw73

Specify the image:
```yml
image: registry.gitlab.com/scandyna/docker-images-windows/windows-cpp-win32_mingw73:latest
```

gcc 7.3 32bit is installed in `C:\Qt\Tools\mingw730_32\bin` and is in the `PATH`.

### windows-cpp-win64_mingw81

Specify the image:
```yml
image: registry.gitlab.com/scandyna/docker-images-windows/windows-cpp-win64_mingw81:latest
```

gcc 8.1 64bit is installed in `C:\MinGW\mingw64\bin` and is in the `PATH`.

Note that no additional conan remote is configured,
the user project has to add the required ones.

### windows-win64-mingw13

Specify the image:
```yml
image: registry.gitlab.com/scandyna/docker-images-windows/windows-win64-mingw13:latest
```

gcc 13 64 bit is installed and is in the `PATH`.

The installed variant is:
```
threads: posix
exception: seh
runtime: ucrt
```

Note that no additional conan remote is configured,
the user project has to add the required ones.

### windows-cpp-msvc2019

Specify the image:
```yml
image: registry.gitlab.com/scandyna/docker-images-windows/windows-cpp-msvc2019:latest
```

MSVC 2019 is default installed.
CMake should be able to find it.

Note that no additional conan remote is configured,
the user project has to add the required ones.

## Images to build Qt

This image is dedicated to build Qt itself.
See https://gitlab.com/scandyna/conan-qt-builds/-/issues/1
(To build Qt for MSVC, no special image is required)

### windows-build-qt-win64_mingw81

Specify the image:
```yml
image: registry.gitlab.com/scandyna/docker-images-windows/windows-build-qt-win64_mingw81:latest
```

gcc 8.1 64bit is installed in `C:\MinGW\mingw64\bin` and is in the `PATH`.

Note that no additional conan remote is configured,
the user project has to add the required ones.

### windows-build-qt-win64-mingw13

Specify the image:
```yml
image: registry.gitlab.com/scandyna/docker-images-windows/windows-build-qt-win64-mingw13:latest
```

gcc 13 64 bit is installed and is in the `PATH`.

A Conan profile, named `windows_gcc13_x86_64_env.jinja`, is generated.
This profile can be used to build some packages having problematic build requirements,
like `strawberryperl` or `MSYS2`.
For more details, see https://gitlab.com/scandyna/docker-images-windows/-/issues/12 .

As an example, we use a profile to build Qt, named `windows_gcc13_x86_64_build_qt` :
```
include(windows_gcc13_x86_64)
include(windows_gcc13_x86_64_env.jinja)
include(qt_widgets_modules)
```

Note that this `windows_gcc13_x86_64_build_qt` should be used in both `build` and `host` context
when building libraries like Qt natively (to build tools like `syncqt`, `moc`, ...).

The installed variant is:
```
threads: posix
exception: seh
C runtime: ucrt
```

Note that no additional conan remote is configured,
the user project has to add the required ones.


## Images having some libraries (deprecated)

Some images that also have some libraries,
like Boost and Qt.

Images with libraries are big,
and there will always be a missing library.
This does not scale.

It is recommanded to use a package manager, like Conan,
to deal with dependencies.

### windows-cpp-qt-5.14.2-win64_mingw73

Specify the image:
```yml
image: registry.gitlab.com/scandyna/docker-images-windows/windows-cpp-qt-5.14.2-win64_mingw73:latest
```

gcc 7.3 64bit is installed in `C:\Qt\Tools\mingw730_64\bin` and is in the `PATH`.

Qt 5.14.2 is installed in `C:\Qt\5.14.2\mingw73_64`.

### windows-cpp-qt-5.14.2-win32_mingw73

Specify the image:
```yml
image: registry.gitlab.com/scandyna/docker-images-windows/windows-cpp-qt-5.14.2-win32_mingw73:latest
```

gcc 7.3 32bit is installed in `C:\Qt\Tools\mingw730_32\bin` and is in the `PATH`.

Qt 5.14.2 is installed in `C:\Qt\5.14.2\mingw73_32`.

### windows-cpp-qt-5.14.2-win64_msvc2017_64

Specify the image:
```yml
image: registry.gitlab.com/scandyna/docker-images-windows/windows-cpp-qt-5.14.2-win64_msvc2017_64:latest
```

MSVC 2019 is default installed.
CMake should be able to find it.

Qt 5.14.2 is installed in `C:\Qt\5.14.2\msvc2017_64`
