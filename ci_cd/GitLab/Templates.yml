
include:
  - local: 'ci_cd/GitLab/Conan_Templates.yml'

###############################
# Runners + global parts
###############################

# GitLab shared new Windows runners
# They currently not support Docker engine
#.windows_runner:
  #tags:
    #- shared-windows
    #- windows
    #- windows-1809

#.windows_runner:
  #tags:
    #- windows-aws

.windows_runner:
  tags:
    - windows-shell

.windows_runner_docker:
  tags:
    - docker-windows


variables:
  DOCKER_REGISTRY_BASE_PATH: registry.gitlab.com/scandyna/docker-images-windows

###############################
# Reusable templates
###############################

.build_rules:
  rules:
    - changes:
      - docker/**/*
      - tests/**/*
      - ci_cd/**/*
      - .gitlab-ci.yml
      when: on_success

# See: https://docs.docker.com/develop/develop-images/dockerfile_best-practices/
#  Typicall: build context
.build:
  rules:
    - !reference [.build_rules, rules]
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker version
    - docker system prune -f
    - cd docker
    - docker build --pull -f ${DOCKER_IMAGE_BASE_NAME}/Dockerfile -t ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:building .
    - docker push ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:building
  after_script:
    - docker logout $CI_REGISTRY
  extends:
    - .windows_runner


# Test for minimal images
# This should at least catch errors like:
#  https://gitlab.com/scandyna/docker-images-windows/-/issues/2
.test_minimal:
  stage: test
  rules:
    - !reference [.build_rules, rules]
  image: ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:building
  extends:
    - .windows_runner_docker
  script:
    - powershell tests/scripts/windows_print_env.ps1
    - pwsh --version


# NOTE: conan config get will not display items from global.conf with conan 1
#       But, it will report an error if an item is wrong in it
.test_tools_script:
  script:
    - cmake --version
    - powershell tests/scripts/windows_print_env.ps1
    - python --version
    - pip --version
    - conan --version
    - conan remote list
    - conan config get
    - conan profile list
    - git --version
    - 7z
    - pwsh --version

.test_tools:
  stage: test
  rules:
    - !reference [.build_rules, rules]
  image: ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:building
  extends:
    - .test_tools_script
    - .windows_runner_docker

.test_tools_Gcc:
  stage: test
  rules:
    - !reference [.build_rules, rules]
  image: ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:building
  extends: .windows_runner_docker
  script:
    - !reference [.test_tools_script, script]
    - gcc --version

# DEPRECATED
.test_tools_GccAndQt:
  stage: test
  rules:
    - !reference [.build_rules, rules]
  image: ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:building
  extends: .windows_runner_docker
  script:
    - C:\Qt\Tools\mingw730_64\bin\gcc.exe --version
    - C:\Qt\Tools\mingw730_32\bin\gcc.exe --version
    - powershell $env:QT_PREFIX_PATH/bin/qmake.exe -version


# NOTE: the CMAKE_GENERATOR and related are provided as variables
# (that are environment variables, supported by CMake since 3.15)
.build_SimpleApp:
  stage: test
  image: ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:building
  extends: .windows_runner_docker
  script:
    - mkdir build
    - cd build
    - cmake -DCMAKE_BUILD_TYPE=Debug "-DCMAKE_TOOLCHAIN_FILE=../tests/cmake-toolchains/${COMPILER}_x86_64.cmake" ../tests/SimpleApp
    - cmake --build . --config Debug -j2
    - ctest --output-on-failure -C Debug

# DEPRECATED see https://gitlab.com/scandyna/docker-images-windows/-/issues/8
#.test_script:
  #before_script:
    #- $env:PATH += ";$QT_PREFIX_PATH/bin"
  #script:
    #- mkdir build
    #- cd build
    #- cmake -G"$CMAKE_GENERATOR" $CMAKE_GENERATOR_EXTRA_ARGS $CMAKE_OPTIONS -DCMAKE_BUILD_TYPE=$BUILD_TYPE
            #-DCMAKE_PREFIX_PATH="$BOOST_PREFIX_PATH;$QT_PREFIX_PATH" ../tests
    #- cmake --build . --config $BUILD_TYPE
    #- qtdiag --fonts
    #- ctest --output-on-failure --verbose --build-config $BUILD_TYPE .

# DEPRECATED see https://gitlab.com/scandyna/docker-images-windows/-/issues/8
#.test:
  #stage: test
  #rules:
    #- !reference [.build_rules, rules]
  #image: ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:building
  #extends:
    #- .test_script
    #- .windows_runner_docker

# TODO: maybe to complex here
.test_QtProject_Conan:
  stage: test
  rules:
    - !reference [.build_rules, rules]
  image: ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:building
  extends:
    - .windows_runner_docker
  before_script:
    - !reference [.setup_conan, script]
  script:
    - mkdir build
    - cd build
    - conan install
      --profile:build $CONAN_PROFILE_BUILD --profile:host $CONAN_PROFILE_HOST
      --settings:build build_type=Release --settings:host build_type=$BUILD_TYPE
      ../tests
    - .\conanbuild
    # The toolchain file expression has to be quoted when using Powershell
    # See https://gitlab.kitware.com/cmake/cmake/-/issues/23109
    - cmake "-DCMAKE_TOOLCHAIN_FILE=conan_toolchain.cmake"
      "-DCMAKE_BUILD_TYPE=$BUILD_TYPE"
      -DTEST_QT_WIDGETS=ON
      -DCMAKE_MESSAGE_LOG_LEVEL=DEBUG
      ../tests
    - cmake --build . --config $BUILD_TYPE
    - .\deactivate_conanbuild
    - ctest --output-on-failure --verbose -C "$BUILD_TYPE"
  artifacts:
    when: on_failure
    expire_in: 1 week
    paths:
      - build


.deploy:
  stage: deploy
  rules:
    - if: $CI_COMMIT_TAG
      when: on_success
    - when: never
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker pull ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:building
    - docker tag ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:building ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:${CI_COMMIT_REF_NAME}
    - docker tag ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:${CI_COMMIT_REF_NAME} ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:latest
    - docker push ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:${CI_COMMIT_REF_NAME}
    - docker push ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:latest
  after_script:
    - docker logout $CI_REGISTRY
  extends:
    - .windows_runner

# TODO remove
.test_deployed_base:
  stage: test_deployed
  rules:
    - if: $CI_COMMIT_TAG
      when: on_success
    - when: never

# TODO remove
.test_tools_deployed:
  image: ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:latest
  extends:
    - .test_deployed_base
    - .test_tools_script
    - .windows_runner_docker

# TODO remove
.test_tools_Gcc_deployed:
  image: ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:latest
  extends:
    - .windows_runner_docker
    - .test_deployed_base
  script:
    - !reference [.test_tools_script, script]
    - gcc --version

# DEPRECATED see https://gitlab.com/scandyna/docker-images-windows/-/issues/8
#.test_deployed:
  #image: ${DOCKER_REGISTRY_BASE_PATH}/${DOCKER_IMAGE_BASE_NAME}:latest
  #extends:
    #- .test_deployed_base
    #- .test_script
    #- .windows_runner_docker
